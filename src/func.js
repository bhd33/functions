const listOfPosts2 = [
    {
        id: 1,
        post: 'some post1',
        title: 'title 1',
        author: 'Rimus',
        comments: [
            {
                id: 1.1,
                comment: 'some comment1',
                title: 'title 1',
                author: 'Ivanov',
            },
            {
                id: 1.2,
                comment: 'some comment2',
                title: 'title 2',
                author: 'Uncle',
            },
        ],
    },
    {
        id: 2,
        post: 'some post2',
        title: 'title 2',
        author: 'Ivanov',
        comments: [
            {
                id: 1.1,
                comment: 'some comment1',
                title: 'title 1',
                author: 'Rimus',
            },
            {
                id: 1.2,
                comment: 'some comment2',
                title: 'title 2',
                author: 'Uncle',
            },
            {
                id: 1.3,
                comment: 'some comment3',
                title: 'title 3',
                author: 'Ivanov',
            },
        ],
    },
    {
        id: 3,
        post: 'some post3',
        title: 'title 3',
        author: 'Rimus',
    },
    {
        id: 4,
        post: 'some post4',
        title: 'title 4',
        author: 'Uncle',
    },
];


const getSum = (str1, str2) => {
    if (typeof str1 === 'string' && typeof str2 === 'string' && !isNaN(str1) && !isNaN(str2)){
        return String(Number(str1) + Number(str2));
    }
    return false;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let numOfPosts = listOfPosts.filter((el) => {
        return el.author === authorName;
    }).length;

    let numOfComments = listOfPosts.reduce((num, el) => {
        if (el.hasOwnProperty('comments')){
            return num + el.comments.filter((elem) => {
                return elem.author === authorName;
            }).length;
        } else {
            return num;
        }
    }, 0);

    return `Post:${numOfPosts},comments:${numOfComments}`
};

const tickets = (people) => {
    let ticketPrice = 25;
    let currentMoney = {
        25: 0,
        50: 0,
        100: 0
    };

    for (const person of people) {
        if (person === ticketPrice){
            currentMoney[person]++;
        }else{
            let curr = person - ticketPrice;
            while (!currentMoney.hasOwnProperty(String(curr))){
                if (currentMoney[ticketPrice] !== 0){
                    currentMoney[ticketPrice]--;
                }else{
                    return 'NO';
                }
                curr -= ticketPrice;
            }
            if (currentMoney[curr] !== 0){
                currentMoney[curr]--;
            }else{
                return 'NO';
            }
            currentMoney[person]++;
        }
    }
    return 'YES';

};
// console.log(getSum(`123maxim`, `3coding24`)) // ->  fasle
// console.log(getSum(``, `4444`))  //->  '4444'
// console.log(getSum(`123`, `324`))   //->  ‘447’
// console.log(getSum(`111111111111111111111111111111111111111111111111111`, `23333333333333333333333333333333333333333333333333`))
// console.log(tickets([25, 25, 50])); // => 'YES'
// console.log(tickets([25, 100]));    // => 'NO'
// console.log(tickets([25, 25, 50, 100])); // 'YES'
// console.log(tickets([25, 50, 100])); // 'NO'
// console.log(tickets([`25`, `25`, `50`, `100`])); // 'YES'
// console.log(tickets([`25`, `50`, `100`])); // 'NO'
console.log(getQuantityPostsByAuthor(listOfPosts2, 'Rimus'))
module.exports = {getSum, getQuantityPostsByAuthor, tickets};
